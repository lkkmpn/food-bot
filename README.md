# Foodbot

![robot in front of plate, holding knife and fork](u1f916_u1f37d-ufe0f.png){height=128px}

Foodbot is a Discord bot for scheduled posting of content scraped from
webpages. It was originally created to post the menus of various canteens at
Aarhus University, but can probably be used to work for many other things.

## Installation

```bash
$ git clone https://gitlab.com/lkkmpn/food-bot.git
$ pip install ./food-bot
```

To run as a service, copy `food-bot.service` to
`/etc/systemd/system/food-bot.service`. Change paths in that file where
necessary.

Configuration is managed in a `.env` file. Copy `example-config.env` to `.env`
and change any configuration values. See
[`food_bot/config.py`](food_bot/config.py) for explanations of all
configuration keys.

## License

Foodbot is licensed under [the MIT license.](LICENSE.md)

The robot artwork is from
[Google's Emoji Kitchen,](https://twitter.com/emojikitchen) based on
[Noto Color Emoji.](https://fonts.google.com/noto/specimen/Noto+Color+Emoji)
