import dataclasses
import datetime
from dataclasses import dataclass

from dotenv import dotenv_values


@dataclass(frozen=True)
class Config:
    discord_token: str
    '''Discord bot token.'''

    discord_admin_guild: int
    '''Discord guild ID to enable admin commands in.'''

    discord_admin_channel: int
    '''Discord channel ID to send administrative messages to.'''

    discord_message_guild: int
    '''Discord guild ID to enable user commands in.'''

    discord_message_channel: int
    '''Discord channel ID to send record messages to.'''

    http_user_agent: str
    '''User agent to use in HTTP requests.'''

    loop_time: datetime.time
    '''Time of day at which to post an automated message.'''

    deepl_api_domain: str
    '''DeepL API domain.'''

    deepl_api_auth_key: str
    '''DeepL API authentication key.'''

    converters = {
        'loop_time': datetime.time.fromisoformat
    }

    @classmethod
    def from_dotenv(cls, **kwargs):
        """Create a configuration from dotenv values.
        """
        values = dotenv_values(**kwargs)
        values = {k.lower(): v for k, v in values.items()}
        return cls(**values)

    def __post_init__(self):
        """Convert configuration fields to the types defined in the dataclass.
        """
        for field in dataclasses.fields(self):
            value = getattr(self, field.name)

            # note that `field.type` is only a `type` object if `annotations`
            # is not imported from `__future__`
            if not isinstance(value, field.type):
                try:
                    if field.name in self.converters:
                        converter = self.converters.get(field.name)
                        converted = converter(value)
                    else:
                        converted = field.type(value)
                except:
                    raise TypeError(f'Cannot convert "{value}" into {field.type.__name__} '
                                    f'(configuration key "{field.name}")')

                # `object.__setattr__` to work around `frozen=True`
                object.__setattr__(self, field.name, converted)


config = Config.from_dotenv()
'''Configuration values for the bot.'''
