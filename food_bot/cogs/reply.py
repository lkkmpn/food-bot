from __future__ import annotations

import logging
import random
import traceback
import typing
from contextlib import asynccontextmanager

from discord.emoji import Emoji
from discord.ext.commands import Cog
from discord.message import Message
from discord.partial_emoji import PartialEmoji

from food_bot.config import config

if typing.TYPE_CHECKING:
    from food_bot.bot import Bot


class ReplyCog(Cog):
    def __init__(self, bot: Bot):
        """Cog to handle replies to user behavior.

        Parameters
        ----------
        bot : Bot
            Discord bot object.
        """
        self.bot = bot

        self.logger = logging.getLogger('foodbot.reply')

        self.logger.info('Initializing reply cog')

    @asynccontextmanager
    async def _log_exception(self):
        """Log exceptions that occur when handling events.
        """
        try:
            yield
        except Exception:
            msg = 'Exception in reply cog!'
            self.logger.error(msg + '\n' + traceback.format_exc().strip())

            try:
                channel = self.bot.get_channel(config.discord_admin_channel)
                await channel.send(msg + f'\n```{traceback.format_exc()}```')
            except Exception:
                pass

    @Cog.listener()
    async def on_message(self, message: Message):
        """Listen to messages.

        Parameters
        ----------
        message : Message
            Message.
        """
        async with self._log_exception():
            if message.author.id == self.bot.user.id:
                return

            content = message.content.lower()

            react_emoji = ['❤️', '🩷', '🧡', '💛', '💚', '💙', '🩵', '💜', '🤎', '🖤', '🩶', '🤍',
                           '💘', '💝', '💖', '💓', '💞', '💕', '💟', '❣️', '❤️‍🔥', '💋',
                           '🫨', '🤮']

            if self.bot.user in message.mentions or \
                    (message.reference is not None and
                     isinstance(message.reference.resolved, Message) and
                     message.reference.resolved.author == self.bot.user):
                if any(word in content for word in ['good bot', 'thank']):
                    reaction = random.choice(['😁', '😊', '😇', '🥰', '😍', '🤩', '☺️',
                                              '😚', '🫨'])
                    await self.try_add_reaction(message, reaction)
                elif 'bad bot' in content:
                    reaction = random.choice(['🙃', '🫠', '😐', '😑', '😶', '🫥', '🤮',
                                              '😟', '😰', '😢', '😭', '😖', '😖', '😡',
                                              '😡'])
                    await self.try_add_reaction(message, reaction)
                elif any(emoji in content for emoji in react_emoji):
                    emojis_in_content_dict = {content.index(emoji): emoji for emoji in react_emoji if emoji in content}
                    emojis_in_content = [i[1] for i in sorted(emojis_in_content_dict.items(), key=lambda i: i[0])]
                    for emoji in emojis_in_content:
                        await self.try_add_reaction(message, emoji)
                elif any(word in content for word in ['hello', 'hi', 'hey', 'hej', 'sup']):
                    reply = random.choice(['Hello!', 'Hi!', 'Hey!', 'Hej!']) + ' 👋'
                    await message.reply(content=reply)

    async def try_add_reaction(self, message: Message, emoji: Emoji | PartialEmoji | str) -> None:
        """Try to add a reaction to a message, but fail silently if an
        exception is raised.

        Parameters
        ----------
        message : Message
            Message to add the reaction to.
        emoji : Emoji | PartialEmoji | str
            Reaction to add.
        """
        try:
            return await message.add_reaction(emoji)
        except Exception:
            pass
