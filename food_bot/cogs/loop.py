from __future__ import annotations

import asyncio
import datetime
import logging
import traceback
import typing

from discord.ext.commands import Cog

from food_bot.config import config
from food_bot.response.response import create_response

if typing.TYPE_CHECKING:
    from food_bot.bot import Bot


class LoopCog(Cog):
    def __init__(self, bot: Bot):
        """Cog to run the automated message loop.

        Parameters
        ----------
        bot : Bot
            Discord bot object.
        """
        self.bot = bot

        self.logger = logging.getLogger('foodbot.loop')

        self.logger.info('Initializing loop log')

        self.bot.loop.create_task(self.loop())

    async def loop(self):
        """Main loop.
        """
        # wait until the bot is ready
        while not self.bot.is_ready():
            await asyncio.sleep(1)

        while True:
            # wait until the next loop time
            now = datetime.datetime.now()
            now_time = now.time()
            if now_time < config.loop_time:
                next_loop_time = datetime.datetime.combine(now.date(), config.loop_time)
            else:
                next_loop_time = datetime.datetime.combine(now.date() + datetime.timedelta(days=1), config.loop_time)

            self.logger.info(f'Waiting until {next_loop_time.isoformat()}')

            await asyncio.sleep((next_loop_time - now).total_seconds())

            try:
                await self.loop_iteration()
            except Exception:
                msg = 'Exception in loop!'
                self.logger.error(msg + '\n' + traceback.format_exc().strip())

                try:
                    channel = self.bot.get_channel(config.discord_admin_channel)
                    await channel.send(msg + f'\n```{traceback.format_exc()}```')
                except Exception:
                    pass

    async def loop_iteration(self):
        """Iteration of the message loop.
        """
        self.logger.info('Loop iteration')

        response = await create_response(datetime.date.today())

        if response is not None:
            self.logger.info('Posting message')

            channel = self.bot.get_channel(config.discord_message_channel)
            await channel.send(**response)
