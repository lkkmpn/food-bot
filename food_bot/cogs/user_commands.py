from __future__ import annotations

import datetime
import logging
import timeit
import traceback
import typing
from contextlib import asynccontextmanager

import discord
from discord.commands.context import ApplicationContext
from discord.embeds import Embed
from discord.ext.commands import Cog

from food_bot.config import config
from food_bot.response.response import create_response

if typing.TYPE_CHECKING:
    from food_bot.bot import Bot


class UserCommandsCog(Cog):
    def __init__(self, bot: Bot):
        """Cog to handle user slash commands.

        Parameters
        ----------
        bot : Bot
            Discord bot object.
        """
        self.bot = bot

        self.logger = logging.getLogger('foodbot.user_commands')

        self.logger.info('Initializing user commands cog')

    @asynccontextmanager
    async def _log_slash_command(self, ctx: ApplicationContext):
        """Run a slash command while logging it and handling exceptions.

        Parameters
        ----------
        ctx : ApplicationContext
            Slash command context.
        """
        data = ctx.interaction.data
        name = data.get('name')
        options = data.get('options', {})

        options_str = ', '.join(f'{opt.get("name")}="{opt.get("value")}"' for opt in options)
        command_str = f'{name}({options_str})'

        self.logger.info(f'{ctx.interaction.user.name} invoked command {command_str}')

        start_time = timeit.default_timer()

        try:
            yield
        except Exception:
            msg = 'Exception in slash command!'
            self.logger.error(msg + '\n' + traceback.format_exc().strip())

            try:
                channel = self.bot.get_channel(config.discord_admin_channel)
                await channel.send(msg + f'\n```{traceback.format_exc()}```')
            except Exception:
                pass
        finally:
            end_time = timeit.default_timer()
            dt = end_time - start_time

            self.logger.info(f'{command_str} executed in {dt:.3f} s')

    @discord.slash_command(name='menu',
                           description='Show today\'s menu',
                           guild_ids=[config.discord_message_guild])
    async def menu(self, ctx: ApplicationContext):
        """Slash command to show today's menu as an ephemeral message.

        Parameters
        ----------
        ctx : ApplicationContext
            Slash command context.
        """
        async with self._log_slash_command(ctx):
            response = await create_response(datetime.date.today())

            if response is not None:
                await ctx.respond(**response, ephemeral=True)
            else:
                await ctx.respond(content='No menu items found.', ephemeral=True)

    @discord.slash_command(name='cake',
                           description='Let the group know you desire cake',
                           guild_ids=[config.discord_message_guild])
    async def cake(self, ctx: ApplicationContext):
        """Slash command to send a message saying someone desires cake.

        Parameters
        ----------
        ctx : ApplicationContext
            Slash command context.
        """
        async with self._log_slash_command(ctx):
            embed = Embed()

            embed.title = '🍰 Cake run'
            embed.description = 'Someone in the group wants to go for a cake run!'

            await ctx.respond(content='I will let everyone know.', ephemeral=True)
            await ctx.channel.send(embed=embed)
