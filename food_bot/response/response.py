import dataclasses
from collections.abc import Mapping
from dataclasses import dataclass
from datetime import date
from typing import Type

from discord.embeds import Embed
from discord.ui.view import View

from food_bot.response.translator import TranslatorView
from food_bot.sources.base import SourceBase
from food_bot.sources.sources import sources
from food_bot.utils import strftime


@dataclass
class Response(Mapping):
    embed: Embed
    view: View

    def __iter__(self):
        for field in dataclasses.fields(self):
            yield field.name

    def __getitem__(self, item):
        return getattr(self, item)

    def __len__(self):
        return len(dataclasses.fields(self))


async def create_response(date: date) -> Response | None:
    """Create the response containing data from all sources for a given date.

    Parameters
    ----------
    date : date
        Date to get data for.

    Returns
    -------
    Response | None
        Formatted response, or None if no sources contained data.
    """
    embed = Embed()

    embed.title = f'Menu for {strftime(date, "%A %-d %B %Y")}'

    all_data: dict[Type[SourceBase], str] = {}

    for source_cls in sources:
        source = source_cls()
        data = await source.get_data(date)
        if data is None:
            continue

        field = await source.get_embed_field(data)
        field.inline = False

        embed.append_field(field)

        all_data[source_cls] = data

    if len(embed.fields) == 0:
        return None

    view = TranslatorView(data=all_data)

    return Response(embed=embed, view=view)
