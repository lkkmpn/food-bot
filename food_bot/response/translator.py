import json
import logging
import timeit
import traceback
from contextlib import asynccontextmanager
from typing import Type

from discord.embeds import Embed
from discord.enums import ButtonStyle
from discord.interactions import Interaction
from discord.ui.button import Button, button
from discord.ui.item import Item
from discord.ui.view import View

from food_bot.config import config
from food_bot.sources.base import SourceBase
from food_bot.utils import http_post


class TranslatorView(View):
    def __init__(self,
                 *items: Item,
                 data: dict[Type[SourceBase], str],
                 timeout: float | None = 8 * 60 * 60,
                 disable_on_timeout: bool = True):
        """Create a TranslatorView to handle translations.

        Parameters
        ----------
        data : dict[Type[SourceBase], str]
            Data from the sources.
        """
        super().__init__(*items, timeout=timeout, disable_on_timeout=disable_on_timeout)

        self.logger = logging.getLogger('foodbot.translator_view')

        self.data = data

    @asynccontextmanager
    async def _log_view_interaction(self, interaction: Interaction):
        """Run the interaction while logging it and handling exceptions.

        Parameters
        ----------
        interaction : Interaction
            Interaction.
        """
        self.logger.info(f'{interaction.user.name} invoked translation')

        start_time = timeit.default_timer()

        try:
            yield
        except Exception:
            msg = 'Exception in translator view!'
            self.logger.error(msg + '\n' + traceback.format_exc().strip())

            try:
                channel = self.bot.get_channel(config.discord_admin_channel)
                await channel.send(msg + f'\n```{traceback.format_exc()}```')
            except Exception:
                pass
        finally:
            end_time = timeit.default_timer()
            dt = end_time - start_time

            self.logger.info(f'Translation executed in {dt:.3f} s')

    @button(label='Translate', style=ButtonStyle.secondary)
    async def button_callback(self, _: Button, interaction: Interaction):
        """Button to translate today's menu and return it as an ephemeral
        message.

        Parameters
        ----------
        _ : Button
            Clicked button.
        interaction : Interaction
            Interaction.
        """
        async with self._log_view_interaction(interaction):
            texts = list(self.data.values())

            if interaction.user.id == 697795967298043996:  # Jendrik
                target_lang = 'DE'
                footer_text = 'Übersetzt mit DeepL'
            else:
                target_lang = 'EN-GB'
                footer_text = 'Translation powered by DeepL'

            translated_texts = await deepl_translate(texts, 'DA', target_lang)

            translated_data = {source_cls: text for source_cls, text in zip(self.data.keys(), translated_texts)}

            embed = Embed()

            for source_cls, data in translated_data.items():
                source = source_cls()

                field = await source.get_embed_field(data)
                field.inline = False

                embed.append_field(field)

            embed.set_footer(text=footer_text)

            await interaction.response.send_message(embed=embed, ephemeral=True)


async def deepl_translate(texts: list[str], source_lang: str, target_lang: str) -> list[str]:
    """Translate a list of texts using the DeepL API.

    Parameters
    ----------
    texts : list[str]
        Texts to translate.
    source_lang : str
        Source language.
    target_lang : str
        Target language.

    Returns
    -------
    list[str]
        Translated texts.
    """
    response = await http_post(
        f'{config.deepl_api_domain}/v2/translate',
        headers={
            'Authorization': f'DeepL-Auth-Key {config.deepl_api_auth_key}',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data={
            'text': texts,
            'source_lang': source_lang,
            'target_lang': target_lang,
            'preserve_formatting': True,
            'split_sentences': 1  # split on punctuation and on newlines
        })

    response = json.loads(response)

    return [translation.get('text') for translation in response.get('translations')]
