from food_bot.bot import Bot
from food_bot.config import config
from food_bot.log import setup_logging


def cli_entry_point():
    setup_logging()

    bot = Bot()
    bot.run(config.discord_token)
