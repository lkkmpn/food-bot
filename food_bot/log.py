import logging
import logging.handlers


def setup_logging() -> None:
    """Set up the logger for the program.
    """
    logger = logging.getLogger()

    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)s [%(name)s] %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    fh = logging.handlers.RotatingFileHandler(
        filename='food-bot.log',
        encoding='utf-8',
        maxBytes=2 * 1024 * 1024,  # 2 MB
        backupCount=5
    )
    fh.setLevel(logging.INFO)
    fh.setFormatter(formatter)

    logger.addHandler(ch)
    logger.addHandler(fh)
    logger.setLevel(logging.INFO)
    logger.setLevel(logging.INFO)
