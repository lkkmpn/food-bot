from datetime import date

from food_bot.sources.base import SourceBase


class Test(SourceBase):
    name = 'Test'

    async def get_data(self, date: date) -> str | None:
        """Get data for a given date.

        Parameters
        ----------
        date : date
            Date to get data for.

        Returns
        -------
        str | None
            Data to place in the embed, or None if this source contains no
            data.
        """
        return f'Hello, World ({date.isoformat()})'
