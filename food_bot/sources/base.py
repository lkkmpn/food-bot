from abc import ABC, abstractmethod
from datetime import date

from discord.embeds import EmbedField


class SourceBase(ABC):
    """Base class for a source.
    """
    @property
    @abstractmethod
    def name(self) -> str:
        """Name of the source, used as title in the embed field.
        """
        ...

    @abstractmethod
    async def get_data(self, date: date) -> str | None:
        """Get data for a given date.

        Parameters
        ----------
        date : date
            Date to get data for.

        Returns
        -------
        str | None
            Data to place in the embed, or None if this source contains no
            data.
        """
        ...

    async def get_embed_field(self, data: str) -> EmbedField:
        """Get the embed field to include in the Discord embed.

        Parameters
        ----------
        data : str
            Data to place in the embed field.

        Returns
        -------
        EmbedField
            Embed field to place in the embed.
        """
        name = self.name
        value = data

        return EmbedField(name=name, value=value)
