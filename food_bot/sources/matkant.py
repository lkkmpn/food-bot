import re
from datetime import date

from bs4 import BeautifulSoup
from bs4.element import Comment, Tag

from food_bot.sources.base import SourceBase
from food_bot.utils import http_get


class Matkant(SourceBase):
    name = 'Matkant'

    async def get_data(self, date: date) -> str | None:
        """Get data for a given date.

        Parameters
        ----------
        date : date
            Date to get data for.

        Returns
        -------
        str | None
            Data to place in the embed, or None if this source contains no
            data.
        """
        # only Mon-Fri
        weekday_number = date.weekday()
        if weekday_number >= 5:
            return None

        html = await http_get('https://matkant.dk/menu/dag/')

        soup = BeautifulSoup(html, 'html.parser')
        main = soup.find('main')

        # find date headers
        headers: list[Tag] = main.find_all('h3')

        # for every date, get the contents in that block
        dates: dict[int, str] = {}
        for header in headers:
            number = re.search(r'\d+', header.string)
            if number is None:
                continue
            number = int(number.group(0))

            contents = str(header)
            for tag in header.next_siblings:
                if tag.name == 'h3':
                    break
                elif isinstance(tag, Comment):
                    continue
                else:
                    contents += str(tag)

            # we can store day of month as key, as the website never shows more
            # than a few days
            dates[number] = contents

        # get the contents for the current date
        contents = dates.get(date.day)
        if contents is None:
            return None

        contents_soup = BeautifulSoup(contents, 'html.parser')

        # get the dishes
        dishes: list[Tag] = contents_soup.find_all('tr', class_='ret')
        if len(dishes) == 0:
            return None

        items: list[str] = []
        for dish in dishes:
            name_td = dish.find('td', class_='ret_navn')
            name = name_td.find(text=True).strip() if name_td is not None else ''

            price_td = dish.find('td', class_='ret_pris')
            price = price_td.find(text=True).strip() if price_td is not None else ''

            item = f'* {name}: {price}' if price != '' else f'* {name}'
            items.append(item.replace('::', ':'))

        return '\n'.join(items)
