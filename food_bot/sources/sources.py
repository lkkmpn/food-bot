from food_bot.sources.base import SourceBase
from food_bot.sources.library import Library
from food_bot.sources.matkant import Matkant
from food_bot.sources.milk import Milk

sources: list[type[SourceBase]] = [
    Matkant,
    Library,
    Milk
]
'''All sources to get data from.'''
