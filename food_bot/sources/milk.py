from datetime import date

from food_bot.sources.base import SourceBase


class Milk(SourceBase):
    name = '🥛 Milk'

    async def get_data(self, date: date) -> str | None:
        """Get data for a given date.

        Parameters
        ----------
        date : date
            Date to get data for.

        Returns
        -------
        str | None
            Data to place in the embed, or None if this source contains no
            data.
        """
        if date.weekday() != 0:  # Monday
            return None

        return '<@174225741246889984> It is milk time'  # Joe
