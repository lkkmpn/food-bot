from datetime import date

from bs4 import BeautifulSoup
from bs4.element import Tag

from food_bot.sources.base import SourceBase
from food_bot.utils import http_get


class Library(SourceBase):
    name = 'AU Library'

    async def get_data(self, date: date) -> str | None:
        """Get data for a given date.

        Parameters
        ----------
        date : date
            Date to get data for.

        Returns
        -------
        str | None
            Data to place in the embed, or None if this source contains no
            data.
        """
        # get weekday to look for in table
        weekdays = ['Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag']
        weekday_number = date.weekday()
        if weekday_number >= len(weekdays):
            return None
        weekday = weekdays[weekday_number]

        html = await http_get('https://www.kb.dk/besoeg-os/victor-albecks-vej/mad-og-drikke')

        soup = BeautifulSoup(html, 'html.parser')

        # find menu
        header = soup.find(lambda tag: any('Kantinens menuplan' in s for s in tag.strings))

        # find correct row
        table_header = header.parent.find(lambda tag: tag.name == 'th' and
                                          isinstance(tag.string, str) and weekday in tag.string)
        if table_header is None:
            return None

        table_body: Tag | None = table_header.next_sibling
        if table_body is None or 'lukket' in table_body.get_text():
            return None

        dishes = list(table_body.stripped_strings)
        items = [f'* {dish}' for dish in dishes]
        return '\n'.join(items)
