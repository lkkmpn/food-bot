import logging

import discord
from discord.flags import Intents

from food_bot.cogs.loop import LoopCog
from food_bot.cogs.reply import ReplyCog
from food_bot.cogs.user_commands import UserCommandsCog
from food_bot.config import config


class Bot(discord.Bot):
    def __init__(self, **kwargs) -> None:
        discord.VoiceClient.warn_nacl = False
        intents = Intents.default()

        super().__init__(intents=intents, **kwargs)

        self.logger = logging.getLogger('foodbot')

    async def start(self, token: str, *, reconnect: bool = True) -> None:
        self.add_cog(LoopCog(self))

        return await super().start(token, reconnect=reconnect)

    async def on_connect(self) -> None:
        self.add_cog(ReplyCog(self))
        self.add_cog(UserCommandsCog(self))

        await self.sync_commands()

    async def on_ready(self) -> None:
        self.logger.info(f'Logged on as "{self.user}"')

        admin_guild = self.get_guild(config.discord_admin_guild)
        admin_channel = self.get_channel(config.discord_admin_channel)
        message_guild = self.get_guild(config.discord_message_guild)
        message_channel = self.get_channel(config.discord_message_channel)

        self.logger.info(f'Admin guild: {admin_guild.name}')
        self.logger.info(f'Admin channel: {admin_channel.guild.name} → {admin_channel.name}')
        self.logger.info(f'Message guild: {message_guild.name}')
        self.logger.info(f'Message channel: {message_channel.guild.name} → {message_channel.name}')

        activity = discord.Activity(
            name='the menu',
            type=discord.ActivityType.watching
        )

        await self.change_presence(activity=activity)
