import datetime
import os

import aiohttp

from food_bot.config import config


def strftime(d: datetime.datetime | datetime.date | datetime.time,
             fmt: str) -> str:
    """Format a datetime object with the option of removing zero-padding.

    As per https://stackoverflow.com/a/29980406, placing a minus in front of
    the field type removes zero-padding. However, this derives from glibc and
    is thus Linux-specific. On Windows, # is used instead of -
    (https://stackoverflow.com/a/40365192). This function implements this
    feature and is cross-platform.

    Parameters
    ----------
    d : datetime.datetime | datetime.date | datetime.time
        Datetime object to format.
    fmt : str
        Format to use.

    Returns
    -------
    str
        Formatted datetime object.
    """
    if os.name == 'nt':
        fmt = fmt.replace('%-', '%#')

    return d.strftime(fmt)


async def http_get(url: str, **kwargs) -> str:
    """Execute an HTTP GET request using `aiohttp`.

    Parameters
    ----------
    url : str
        URL to GET.

    Returns
    -------
    str
        HTTP response.
    """
    if 'headers' in kwargs:
        kwargs['headers']['User-Agent'] = config.http_user_agent
    else:
        kwargs['headers'] = {'User-Agent': config.http_user_agent}

    async with aiohttp.ClientSession() as session:
        async with session.get(url, **kwargs) as r:
            r.raise_for_status()
            obj = await r.text(encoding='utf-8')

    return obj


async def http_post(url: str, **kwargs) -> str:
    """Execute an HTTP POST request using `aiohttp`.

    Parameters
    ----------
    url : str
        URL to POST.

    Returns
    -------
    str
        HTTP response.
    """
    if 'headers' in kwargs:
        kwargs['headers']['User-Agent'] = config.http_user_agent
    else:
        kwargs['headers'] = {'User-Agent': config.http_user_agent}

    async with aiohttp.ClientSession() as session:
        async with session.post(url, **kwargs) as r:
            r.raise_for_status()
            obj = await r.text(encoding='utf-8')

    return obj
